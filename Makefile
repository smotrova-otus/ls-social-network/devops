BRANCH_NAME = $(shell git rev-parse --abbrev-ref HEAD)

.PHONY: deploy
deploy:
	ansible-playbook -i ./inventory/hosts.yaml ./play-app.yaml -vv

.PHONY: deploy-monitoring
deploy-monitoring:
	ansible-playbook -i ./inventory/hosts.yaml ./play-monitoring.yaml -vv

.PHONY: deploy-infra
deploy-infra:
	ansible-playbook -i ./inventory/hosts.yaml ./play-infra.yaml -vv

.PHONY: deploy-mysql-slaves
deploy-mysql-slaves:
	ansible-playbook -i ./inventory/hosts.yaml ./play-mysql-slaves.yaml -vv

.PHONY: deploy-mysql-master
deploy-mysql-master:
	ansible-playbook -i ./inventory/hosts.yaml ./play-mysql-master.yaml -vv

.PHONY: deploy-mysql-all
deploy-mysql-all:
	ansible-playbook -i ./inventory/hosts.yaml ./play-mysql-all.yaml -vv

.PHONY: deploy-rabbit
deploy-rabbit:
	ansible-playbook -i ./inventory/hosts.yaml ./play-rabbit.yaml -vv

.PHONY: deploy-redis
deploy-redis:
	ansible-playbook -i ./inventory/hosts.yaml ./play-redis.yaml -vv